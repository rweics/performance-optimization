/* CS61C Sp14 Project 3 Part 1: YOUR CODE HERE
 *
 * You MUST implement the calc_min_dist() function in this file.
 *
 * You do not need to implement/use the swap(), flip_horizontal(), transpose(),
 * or rotate_ccw_90() functions, but you may find them useful. Feel free to
 * define additional helper functions.
 */

#include <float.h>
#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include "digit_rec.h"
#include "utils.h"

/* Returns the squared Euclidean distance between TEMPLATE and IMAGE. The size
 * of IMAGE is I_WIDTH * I_HEIGHT, while TEMPLATE is square with side length
 * T_WIDTH. The template image should be flipped, rotated, and translated
 * across IMAGE.
 */
float calc_min_dist(float *image, int i_width, int i_height, float *template,
                    int t_width) {
    float min_dist = FLT_MAX;
    /* YOUR CODE HERE */
    int t_size = t_width * t_width;
    for (int y_offset = 0; y_offset < (i_height - t_width + 1); y_offset++) {
        for (int x_offset = 0; x_offset < (i_width - t_width + 1); x_offset++) {
            float temp_dist[8] = { 0.0 };
            for (int t_index = 0; t_index < t_size; t_index++) {
                int x = t_index % t_width;
                int x_mul_t_wid = x * t_width;
                int y = t_index / t_width;
                int x_1 = t_width - x - 1;
                int x_1_mul_t_wid = x_1 * t_width;
                int y_1 = t_width - y - 1;
                int y_1_mul_t_wid = y_1 * t_width;
                float t = image[x + x_offset + (y + y_offset) * i_width];

                int temp_index = t_index;
                temp_dist[0] += (t - template[temp_index])
                        * (t - template[temp_index]);

                temp_index = x_1_mul_t_wid + y;
                temp_dist[1] += (t - template[temp_index])
                        * (t - template[temp_index]);

                temp_index = y_1_mul_t_wid + x_1;
                temp_dist[2] += (t - template[temp_index])
                        * (t - template[temp_index]);

                temp_index = x_mul_t_wid + y_1;
                temp_dist[3] += (t - template[temp_index])
                        * (t - template[temp_index]);

                temp_index = y * t_width + x_1;
                temp_dist[4] += (t - template[temp_index])
                        * (t - template[temp_index]);

                temp_index = x_1_mul_t_wid + y_1;
                temp_dist[5] += (t - template[temp_index])
                        * (t - template[temp_index]);

                temp_index = y_1_mul_t_wid + x;
                temp_dist[6] += (t - template[temp_index])
                        * (t - template[temp_index]);

                temp_index = x_mul_t_wid + y;
                temp_dist[7] += (t - template[temp_index])
                        * (t - template[temp_index]);
            }
            for (int i = 0; i < 8; i++) {
                if (temp_dist[i] < min_dist) {
                    min_dist = temp_dist[i];
                }
            }
        }
    }
    return min_dist;
}
